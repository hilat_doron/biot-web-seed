exports.format = msgs => {
  const resultData = [];
  for (const [id, obj] of Object.entries(msgs)) {
    resultData.push({
      id,
      ...obj,
    });
  }
  return resultData;
};
