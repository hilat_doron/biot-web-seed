/**
|--------------------------------------------------
| THE DEBUG CONFIG ARE USED FOR CONFIG TO DEBUG TOOLS.
| DONT PUT HERE CONFIG THAT RELATED DIRECTLY TO YOUR APP
|--------------------------------------------------
*/

export default {
  useFixtures: false,
  shouldLog: process.env.NODE_ENV === 'development',
  reduxLogging: process.env.NODE_ENV === 'development',
};
