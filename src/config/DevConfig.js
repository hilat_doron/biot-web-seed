/* eslint import/no-mutable-exports: 0 */
/* eslint prefer-const: 0 */

let DEV_CONFIG = {
  API_URL: 'http://develop-alb-1673772205.us-east-1.elb.amazonaws.com',
  GOOGLE_ANALYTICS_TRACKER_ID: '',
};

export default DEV_CONFIG;
