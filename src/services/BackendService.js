import Axios from 'axios';
import UmsLogic from '@biot/ums-js-logic';
import AppConfig from '../config/AppConfig';

const Resources = {
  loadConfig: {
    uri: 'config/loadConfig/',
    method: 'GET',
    requireAuth: false,
  },
  getUsers: {
    url: 'ums/v1/admin/users/all',
    method: 'GET',
    requireAuth: true,
  },
  getDevices: {
    url: 'targetpoint/device/tenant/',
    method: 'GET',
    requireAuth: true,
  },
  login: {
    url: 'ums/v2/users/login',
    method: 'POST',
    requireAuth: false,
  },
};

/**
|--------------------------------------------------
| The server service - expose interface to the API.
| Every method returns a promise.
|--------------------------------------------------
*/
class BackendService {
  constructor() {
    this._axios = Axios.create({
      baseURL: AppConfig.API_URL,
    });

    this._axios.interceptors.request.use(this._beforeRequestInterceptor, null);
  }

  _beforeRequestInterceptor(request) {
    if (!request.headers?.Authorization && request.requireAuth) {
      const token = `Bearer ${UmsLogic.getToken()}`;

      return {
        ...request,
        headers: { ...request.headers, Authorization: token },
      };
    }

    return request;
  }

  getUsers = () => this._axios.request(Resources.getUsers);

  getDevices = () => this._axios.request(Resources.getDevices);

  login = data => this._axios.request({ ...Resources.login, data }); // TODO: use UMS package to login (when ready + has users)
}

export default new BackendService();
