import { createSlice, createSelector } from '@reduxjs/toolkit';
import _ from 'lodash';

export const STATE_KEY = 'page1';

export const INITIAL_STATE = {
  page1Prop: 'page1Prop',
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {
    page1PropChanged: (state, action) => {
      state.page1Prop = action.payload;
      return state;
    },
  },
});
/* eslint-enable no-param-reassign */

const getState = state => state[STATE_KEY] || INITIAL_STATE;

const getPage1Prop = createSelector(getState, state => _.get(state, 'page1Prop'));

export const selectors = {
  getPage1Prop,
};

export const { actions } = slice;

const { reducer } = slice;
export default reducer;
