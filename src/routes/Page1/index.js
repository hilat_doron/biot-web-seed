import component from './containers/container';
import reducer, { actions, selectors, STATE_KEY } from './modules/slice';

export { actions, reducer, selectors, STATE_KEY };

export default component;
