import React from 'react';
import PropTypes from 'prop-types';

const Page1 = ({ page1Prop }) => (
  <div>
    <div>Page 1</div>
    <div>{`my prop is: ${page1Prop}`}</div>
  </div>
);

Page1.propTypes = {
  page1Prop: PropTypes.string.isRequired,
};

export default Page1;
