import { connect } from 'react-redux';
import { selectors } from '../modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  page1Prop: selectors.getPage1Prop(state),
});

export default connect(mapStateToProps)(Component);
