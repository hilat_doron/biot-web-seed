import { createSlice, createSelector, createAction } from '@reduxjs/toolkit';
import _ from 'lodash';

export const STATE_KEY = 'page3';

export const INITIAL_STATE = {
  page3Prop: 'page3Prop',
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {},
});
/* eslint-enable no-param-reassign */

const extraActions = {
  page3SagaTest: createAction(`${STATE_KEY}/page3SagaTest`),
};

const getState = state => state[STATE_KEY] || INITIAL_STATE;

const getPage3Prop = createSelector(getState, state => _.get(state, 'page3Prop'));

export const selectors = {
  getPage3Prop,
};

export const actions = { ...slice.actions, ...extraActions };

const { reducer } = slice;
export default reducer;
