import { all, takeLatest, delay } from 'redux-saga/effects';
import { actions } from './slice';

function* sagaExample() {
  // Added delay just to have 'yield' to avoid eslint warning.
  yield delay(500);
  alert('Hello from saga example');
}

export default function* watchPage3Actions() {
  yield all([takeLatest(actions.page3SagaTest, sagaExample)]);
}
