import component from './containers/container';
import reducer, { actions, selectors, STATE_KEY } from './modules/slice';
import saga from './modules/saga';

export { actions, reducer, selectors, saga, STATE_KEY };

export default component;
