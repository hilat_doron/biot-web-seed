import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SagaButton = styled.button`
  display: inline;
  padding: 20px;
  border-radius: 20px;
  background-color: lightblue;
  cursor: pointer;
  outline: none;
`;

const Page3 = props => {
  const { page3Prop, page3SagaTest } = props;
  return (
    <div>
      <div>Page 3</div>
      <div>{`my prop is: ${page3Prop}`}</div>
      <br />
      <br />
      <SagaButton onClick={page3SagaTest}>Saga Test</SagaButton>
    </div>
  );
};

Page3.propTypes = {
  page3Prop: PropTypes.string.isRequired,
  page3SagaTest: PropTypes.func.isRequired,
};

export default Page3;
