import { connect } from 'react-redux';
import { selectors, actions } from '../modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  page3Prop: selectors.getPage3Prop(state),
});

const mapActionCreators = {
  ...actions,
};

export default connect(mapStateToProps, mapActionCreators)(Component);
