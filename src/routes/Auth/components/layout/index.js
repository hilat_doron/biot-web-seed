import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import Logo from 'src/components/Logo';
import LanguageMenu from 'src/components/LanguageMenu';

const { Content, Header, Footer } = Layout;

const StyledLayout = styled(Layout)`
  min-height: 100vh;
`;

const MainContent = styled(Content)`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  margin: 0;
  padding: 24px;
  background-color: ${props => props.theme.colors.layoutBackground};
  min-height: 280px;
`;

const StyledHeader = styled(Header)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

function AuthLayout({ children }) {
  return (
    <StyledLayout>
      <StyledHeader>
        <LanguageMenu />
      </StyledHeader>

      <MainContent>
        <Logo />
        {children}
      </MainContent>

      <Footer />
    </StyledLayout>
  );
}

AuthLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthLayout;
