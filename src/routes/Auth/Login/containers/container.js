import { connect } from 'react-redux';
import { selectors as userSelectors } from 'src/redux/data/user/modules/slice';
import { actions } from '../modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  loginStatus: userSelectors.getStatus(state),
});

const mapActionCreators = {
  ...actions,
};

export default connect(mapStateToProps, mapActionCreators)(Component);
