import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Col, Form, Input, Button, Checkbox } from 'antd';
import { injectIntl, defineMessages, FormattedMessage } from 'react-intl';

const Container = styled(Col)`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoginForm = styled(Form)`
  width: 350px;
`;

const LoginButton = styled(Button)`
  width: 100%;
`;

const ErrorText = styled.span`
  color: ${props => props.theme.colors.errorText};
`;

const LoginPage = props => {
  const onFinish = values => {
    const { login } = props;
    login(values);
  };

  const { intl, loginStatus } = props;

  return (
    <Container>
      <LoginForm layout="vertical" onFinish={onFinish}>
        <Form.Item
          label={intl.formatMessage(messages.email)}
          name="email"
          hasFeedback
          rules={[
            {
              type: 'email',
              required: true,
              message: `${intl.formatMessage(messages.invalidEmail)}`,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label={intl.formatMessage(messages.password)}
          name="password"
          hasFeedback
          rules={[
            {
              required: true,
              message: `${intl.formatMessage(messages.requiredPassword)}`,
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item name="rememberMe" valuePropName="checked">
          <Checkbox>
            <FormattedMessage {...messages.rememberMe} />
          </Checkbox>
        </Form.Item>

        <Form.Item>
          <LoginButton type="primary" htmlType="submit">
            <FormattedMessage {...messages.submit} />
          </LoginButton>
        </Form.Item>
        {loginStatus && loginStatus > 200 && (
          <ErrorText>
            <FormattedMessage {...messages.errorText} />
          </ErrorText>
        )}
      </LoginForm>
    </Container>
  );
};

const messages = defineMessages({
  email: {
    defaultMessage: 'Email',
  },
  password: {
    defaultMessage: 'Password',
  },
  errorText: {
    defaultMessage: 'Could not login - wrong email or password',
  },
  invalidEmail: {
    defaultMessage: 'Invalid email address',
  },
  requiredPassword: {
    defaultMessage: 'Password is required',
  },
  rememberMe: {
    defaultMessage: 'Remember me',
  },
  submit: {
    defaultMessage: 'Submit',
  },
});

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  loginStatus: PropTypes.number,
};

LoginPage.defaultProps = {
  loginStatus: null,
};

export default injectIntl(LoginPage);
