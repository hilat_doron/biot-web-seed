import { createSlice, createAction } from '@reduxjs/toolkit';

export const STATE_KEY = 'login';

export const INITIAL_STATE = {
  loginData: null,
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {},
});
/* eslint-enable no-param-reassign */

const extraActions = {
  login: createAction(`${STATE_KEY}/login`),
  loginSuccess: createAction(`${STATE_KEY}/loginSuccess`), // handled by user reducer
  loginFail: createAction(`${STATE_KEY}/loginFail`),
  loginFinish: createAction(`${STATE_KEY}/loginFinish`),
  logout: createAction(`${STATE_KEY}/logout`),
  logoutFinish: createAction(`${STATE_KEY}/logoutFinish`), // handled by user reducer
};

export const actions = { ...slice.actions, ...extraActions };

const { reducer } = slice;
export default reducer;
