import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import { replace, getLocation } from 'connected-react-router';
// import UmsLogicSdk from '@biot/ums-js-logic';
import { getLocaleFromUrlPath } from 'src/utils/languageUtils';
import BackendService from 'src/services/BackendService';
import { actions } from './slice';

function* login(action) {
  const { email, password, rememberMe } = action.payload;

  try {
    const loginResponse = yield call(BackendService.login, {
      username: email,
      password,
      rememberMe,
    });
    yield put(actions.loginSuccess({ data: { ...loginResponse?.data, email }, status: loginResponse?.status }));
    const locationObj = yield select(getLocation);
    yield put(replace(`/${getLocaleFromUrlPath(locationObj.pathname)}/`));
  } catch (e) {
    console.error('error in login: ', e);
    yield put(actions.loginFail(e.response.status));
  }
}

function* logout() {
  try {
    // yield call(UmsLogicSdk.logout); TODO: fix after updating UMS packages
  } catch (e) {
    console.log('Failed to logout: ', e);
  } finally {
    yield put(actions.logoutFinish());
  }
}

export default function* watchLoginActions() {
  yield all([takeLatest(actions.login, login), takeLatest(actions.logout, logout)]);
}
