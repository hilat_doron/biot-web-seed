import { connect } from 'react-redux';
import { selectors as userSelectors } from 'src/redux/data/user/modules/slice';
import { selectors as appSelectors, actions as appActions } from 'src/redux/data/app/modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  isAppLoading: appSelectors.getIsAppLoading(state),
  isLoggedIn: !!userSelectors.getUserId(state),
});

const mapActionCreators = {
  onAppStart: appActions.appStart,
};

export default connect(mapStateToProps, mapActionCreators)(Component);
