import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Spin } from 'antd';
import { ThemeProvider } from 'styled-components';

import theme from 'src/themes/theme';
import PrivateRoute from '../containers/privateRouteContainer';
import AuthRoute from './AuthRoute';

import RedirectToDefaultLocale from './RedirectToDefaultLocale';
import RedirectToSupportedLocale from './RedirectToSupportedLocale';
import { appRoutes } from '../modules/routesUtils';
import { getSupportedLocalesRegularExp, DEFAULT_LOCALE } from '../../../utils/languageUtils';

import Page1 from '../../Page1';
import Page2 from '../../Page2';
import Page3 from '../../Page3';
import Login from '../../Auth/Login';

import { SpinBox } from './styled';
import GlobalStyle from '../../../global-styles';
import LanguageProvider from '../../../components/LanguageProvider';

function Root(props) {
  useEffect(onAppStart, []);

  function onAppStart() {
    props.onAppStart();
  }

  const { isLoggedIn, isAppLoading } = props;

  const _appRoutesSwitch = () => (
    <LanguageProvider>
      <Switch>
        <AuthRoute path={`/:locale/${appRoutes.LOGIN}`} component={Login} />

        <PrivateRoute authed={isLoggedIn} exact path={`/:locale/${appRoutes.PAGE1}`} component={Page1} />
        <PrivateRoute authed={isLoggedIn} exact path={`/:locale/${appRoutes.PAGE2}`} component={Page2} />
        <PrivateRoute authed={isLoggedIn} exact path={`/:locale/${appRoutes.PAGE3}`} component={Page3} />

        {/* default redirection */}
        <Redirect from="/:locale" to={`/:locale/${appRoutes.PAGE1}`} />
      </Switch>
    </LanguageProvider>
  );

  return isAppLoading ? (
    <SpinBox centered>
      <Spin spinning size="large" />
    </SpinBox>
  ) : (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Switch>
        {/* no locale */}
        {RedirectToDefaultLocale()}

        {/* supported locale */}
        <Route sensitive path={`/:locale(${getSupportedLocalesRegularExp()})`} component={_appRoutesSwitch} />

        {/* not supported locale ( wrong syntax / not exist ) */}
        <Route path="/:locale" component={RedirectToSupportedLocale} />

        {/* default redirection */}
        <Redirect from="/*" to={`/${DEFAULT_LOCALE}/`} />
      </Switch>
    </ThemeProvider>
  );
}

Root.propTypes = {
  onAppStart: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool,
  isAppLoading: PropTypes.bool,
};

Root.defaultProps = {
  isLoggedIn: false,
  isAppLoading: false,
};

export default Root;
