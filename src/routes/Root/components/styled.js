import styled from 'styled-components';
import Box from 'src/components/Box';

export const SpinBox = styled(Box)`
  height: 100vh;
`;
