import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import AppLayout from 'src/components/AppLayout';

function PrivateRoute({ component: Component, authed, ...rest }) {
  const {
    computedMatch: {
      params: { locale },
    },
  } = rest;
  const _renderComponent = props => {
    const render = authed ? (
      <AppLayout>
        <Component {...props} />
      </AppLayout>
    ) : (
      <Redirect to={{ pathname: `/${locale}/login`, state: { from: props.location } }} />
    );

    return render;
  };

  return <Route {...rest} render={_renderComponent} />;
}

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node, PropTypes.object]).isRequired,
  location: PropTypes.object,
  authed: PropTypes.bool,
};

PrivateRoute.defaultProps = {
  authed: false,
  location: {},
};

export default PrivateRoute;
