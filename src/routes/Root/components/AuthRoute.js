import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import AuthLayout from '../../Auth/components/layout';

function AuthRoute({ component: Component, /* authed, */ ...rest }) {
  return (
    <Route
      {...rest}
      render={props => (
        <AuthLayout userName="ben">
          <Component {...props} />
        </AuthLayout>
      )}
    />
  );
}

AuthRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node, PropTypes.object]).isRequired,
};

export default AuthRoute;
