import { createSlice, createSelector } from '@reduxjs/toolkit';
import _ from 'lodash';

export const STATE_KEY = 'page2';

export const INITIAL_STATE = {
  page2Prop: 0,
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {
    onPage2PropIncrement: state => {
      state.page2Prop += 1;
      return state;
    },
  },
});
/* eslint-enable no-param-reassign */

const getState = state => state[STATE_KEY] || INITIAL_STATE;

const getPage2Prop = createSelector(getState, state => _.get(state, 'page2Prop'));

export const selectors = {
  getPage2Prop,
};

export const { actions } = slice;

const { reducer } = slice;
export default reducer;
