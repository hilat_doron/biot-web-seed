import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CounterButton = styled.button`
  display: inline;
  padding: 20px;
  border-radius: 20px;
  background-color: lightblue;
  cursor: pointer;
  outline: none;
`;

const Page2 = props => {
  const { page2Prop, onPage2PropIncrement } = props;
  return (
    <div>
      <div>Page 2 is a counter example:</div>
      <br />
      <div>{`my prop is: ${page2Prop}`}</div>
      <br />
      <br />
      <CounterButton onClick={onPage2PropIncrement}>Press to increment prop</CounterButton>
    </div>
  );
};

Page2.propTypes = {
  page2Prop: PropTypes.string.isRequired,
  onPage2PropIncrement: PropTypes.func,
};

Page2.defaultProps = {
  onPage2PropIncrement: null,
};

export default Page2;
