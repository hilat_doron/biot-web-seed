import { connect } from 'react-redux';
import { selectors, actions } from '../modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  page2Prop: selectors.getPage2Prop(state),
});

const mapActionCreators = {
  ...actions,
};

export default connect(mapStateToProps, mapActionCreators)(Component);
