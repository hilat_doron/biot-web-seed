/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import history from 'src/utils/history';
import { reducer as languageMenuReducer, STATE_KEY as languageMenuStateKey } from 'src/components/LanguageMenu';
// import appReducer, { STATE_KEY as appStateKey } from '../modules/app/app.slice';
import { reducer as dataReducer, STATE_KEY as dataStateKey } from '../data';

import page1Reducer, { STATE_KEY as page1StateKey } from '../../routes/Page1/modules/slice';
import page2Reducer, { STATE_KEY as page2StateKey } from '../../routes/Page2/modules/slice';
import page3Reducer, { STATE_KEY as page3StateKey } from '../../routes/Page3/modules/slice';
import loginReducer, { STATE_KEY as loginStateKey } from '../../routes/Auth/Login/modules/slice';

const staticReducers = {
  [languageMenuStateKey]: languageMenuReducer,
  router: connectRouter(history),
  [dataStateKey]: dataReducer,

  // Routes:
  [loginStateKey]: loginReducer,

  [page1StateKey]: page1Reducer,
  [page2StateKey]: page2Reducer,
  [page3StateKey]: page3Reducer,
};

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const createdAppReducer = combineReducers({
    ...staticReducers,
    ...injectedReducers,
  });

  // rootReducer in charge of resetting the store on logout.
  // see https://stackoverflow.com/questions/35622588/how-to-reset-the-state-of-a-redux-store
  /* eslint-disable no-param-reassign */
  const rootReducer = (state, action) => {
    if (action.type === 'some_logout_action') {
      const { router } = state;
      state = { router };
    }
    return createdAppReducer(state, action);
  };
  /* eslint-enable no-param-reassign */

  return rootReducer;
}
