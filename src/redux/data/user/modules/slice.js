import { createSlice, createSelector } from '@reduxjs/toolkit';
import { actions as loginActions } from '../../../../routes/Auth/Login/modules/slice';
import { DATA_STATE_KEY } from '../../constants';

export const STATE_KEY = 'user';

export const INITIAL_STATE = {
  data: {
    email: null,
    name: null,
  },
  status: null,
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {},
  extraReducers: {
    [loginActions.loginSuccess]: (state, action) => {
      const { data, status } = action.payload;
      state.data = data;
      state.status = status;
    },
    [loginActions.loginFail]: (state, action) => {
      state.status = action.payload;
    },
    [loginActions.logoutFinish]: (state, action) => {
      state.data = INITIAL_STATE.data;
    },
  },
});
/* eslint-enable no-param-reassign */

const getState = state => state[DATA_STATE_KEY][STATE_KEY] || INITIAL_STATE;

export const selectors = {
  getLoginData: createSelector(getState, state => state.data),
  getUserEmail: createSelector(getState, state => state.data.email),
  getUserId: createSelector(getState, state => state.data.userId),
  getUsername: createSelector(getState, state => state.data.username),
  getStatus: createSelector(getState, state => state.status),
};

export const { actions } = slice;

const { reducer } = slice;
export default reducer;
