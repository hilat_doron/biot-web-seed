import { all, put, call, takeLatest } from 'redux-saga/effects';
import UmsLogic from '@biot/ums-js-logic';
import { actions as loginActions } from 'src/routes/Auth/Login/modules/slice';
import { actions } from './slice';

function* onAppStart() {
  yield call(handleLogin);

  yield put(actions.appStartFinish());
}

function* handleLogin() {
  const isLoggedIn = yield call(UmsLogic.isLoggedIn);
  const isUserRemembered = yield call(UmsLogic.isUserRemembered);

  if (!isLoggedIn && !isUserRemembered) {
    return;
  }

  try {
    const response = yield call(UmsLogic.loginWithToken);
    yield put(loginActions.loginSuccess(response));
  } catch (e) {
    console.log('Error while trying to login with token: ', e);
  }
}

export default function* watchAppActions() {
  yield all([takeLatest(actions.appStart, onAppStart)]);
}
