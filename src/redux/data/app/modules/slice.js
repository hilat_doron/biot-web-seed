import { createSlice, createSelector, createAction } from '@reduxjs/toolkit';
import _ from 'lodash';

import { DATA_STATE_KEY } from '../../constants';

export const STATE_KEY = 'app';

export const INITIAL_STATE = {
  isAppLoading: true,
};

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: INITIAL_STATE,
  reducers: {
    appStartFinish: (state, action) => {
      state.isAppLoading = action.payload;
      return state;
    },
  },
});
/* eslint-enable no-param-reassign */

// For saga/reducer-less actions
const extraActions = {
  appStart: createAction(`${STATE_KEY}/appStart`),
};

const getState = state => state[DATA_STATE_KEY][STATE_KEY] || INITIAL_STATE;

const getIsAppLoading = createSelector(getState, state => _.get(state, 'isAppLoading'));

export const selectors = {
  getIsAppLoading,
};

export const actions = { ...slice.actions, ...extraActions };

const { reducer } = slice;
export default reducer;
