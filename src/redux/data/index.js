import { combineReducers } from 'redux';
import { fork, all } from 'redux-saga/effects';

import { DATA_STATE_KEY } from './constants';
import { userStateKey, reducer as userReducer } from './user';
import { appStateKey, reducer as appReducer, saga as appSaga } from './app';

export const STATE_KEY = DATA_STATE_KEY;

export const reducer = combineReducers({
  [appStateKey]: appReducer,
  [userStateKey]: userReducer,
});

export const sagas = function* root() {
  yield all([fork(appSaga)]);
};
