import { fork, all } from 'redux-saga/effects';
import { saga as loginSaga } from 'src/routes/Auth/Login';
import { saga as languageSaga } from '../../components/LanguageMenu';
import { sagas as dataSaga } from '../data';

/*
 * The entry point for all general sagas used in this application.
 */
export default function* root() {
  yield all([fork(dataSaga), fork(loginSaga), fork(languageSaga)]);
}
