const palette = {
  primary: '#001529',
  secondary: '#1890ff',
  neutralLight: '#ccc',
  neutralLighter: '#eee',
  neutralLightest: '#fafafa',
  appWhite: '#fff',
  error: '#ff0000',
};

const theme = {
  colors: {
    layoutBackground: palette.appWhite,
    headerBackground: palette.primary,
    headerTextColor: palette.appWhite,
    buttonBackground: palette.secondary,
    languageMenuButtonBackground: palette.neutralLight,
    languageMenuButton: palette.appWhite,
    errorText: palette.error,
    MainContentBackground: palette.neutralLightest,
  },
  fonts: {
    normalFontSize: '14px',
    headerFontSize: '24px',
  },
};

export default theme;
