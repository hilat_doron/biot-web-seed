import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
    overflow: hidden;
  }

  svg { vertical-align: baseline; }

  body {
    //font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    //font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #root {
    background-color: ${props => props.theme.colors.neutralLightest};
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    //font-family: Georgia, Times, 'Times New Roman', serif;
    //line-height: 1.5em;
  }
`;

export default GlobalStyle;
