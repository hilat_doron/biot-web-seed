import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import logo from 'src/images/logo.svg';

import AppVersion from 'src/components/AppVersion';
import Box from 'src/components/Box';

const Version = styled(AppVersion)`
  font-size: 10px;
  color: white;
`;

const normalCSS = css`
  img {
    width: 100%;
    height: 100%;
  }
`;

const smallCSS = css`
  width: 36px;

  img {
    height: 100%;
  }
`;

const ImgContainer = styled(Box).attrs({
  centered: true,
})`
  position: relative;
  text-align: center;
`;

const Mask = styled.div`
  overflow: hidden;
  height: 100%;

  ${props => (props.isSmall ? smallCSS : normalCSS)};
`;

const Logo = props => {
  const { className, isSmall = false } = props;

  return (
    <ImgContainer className={className} small={isSmall}>
      <Mask small={isSmall}>
        <img src={logo} alt="Theranica" />
      </Mask>

      <Version small={isSmall} />
    </ImgContainer>
  );
};

Logo.propTypes = {
  className: PropTypes.string,
  isSmall: PropTypes.bool,
};

Logo.defaultProps = {
  className: null,
  isSmall: false,
};

export default Logo;
