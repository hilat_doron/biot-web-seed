import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Menu, Dropdown, Button } from 'antd';
import { supportedLanguages, getLocaleFromUrlPath, getLanguageByLocale } from '../../../utils/languageUtils';

const MenuButton = styled(Button)`
  background-color: ${props => props.theme.colors.languageMenuButtonBackground};
  color: ${props => props.theme.colors.languageMenuButton};
`;

const LanguageMenu = ({ changeLanguage }) => {
  const [language, setLanguage] = useState(getLanguageByLocale(getLocaleFromUrlPath(window.location.pathname)));

  const handleMenuClick = event => {
    const lang = event.key;
    setLanguage(lang);
    changeLanguage(lang);
  };

  const menu = () => (
    <Menu onClick={handleMenuClick}>
      {Object.values(supportedLanguages).map(lang => (
        <Menu.Item key={lang}>{lang}</Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <MenuButton type="circle">{language}</MenuButton>
    </Dropdown>
  );
};

LanguageMenu.propTypes = {
  changeLanguage: PropTypes.func.isRequired,
};

export default LanguageMenu;
