import component from './containers/container';
import reducer, { actions, STATE_KEY } from './modules/slice';
import saga from './modules/saga';

export { component, actions, reducer, saga, STATE_KEY };

export default component;
