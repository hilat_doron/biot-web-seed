import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Menu } from 'antd';
import { Link as RouterLink } from 'react-router-dom';
import { FormattedMessage, defineMessages } from 'react-intl';
import { HomeOutlined, FundProjectionScreenOutlined, AndroidOutlined } from '@ant-design/icons';

import Box from 'src/components/Box';

const SideMenu = ({ collapsed }) => {
  const [selectedItem, setSelectedItem] = useState(window.location.pathname.split('/')[1] || 'home');
  const onSelect = e => setSelectedItem(e.key);

  return (
    <Menu theme="dark" mode="inline" selectedKeys={[selectedItem]} onSelect={onSelect}>
      <Menu.Item key="home">
        <RouterLink to="/">
          <Box hAligned>
            <HomeOutlined />
            {!collapsed && <FormattedMessage {...messages.home} />}
          </Box>
        </RouterLink>
      </Menu.Item>
      <Menu.Item key="page2">
        <RouterLink to="/page2">
          <Box hAligned>
            <FundProjectionScreenOutlined />
            {!collapsed && <FormattedMessage {...messages.page2} />}
          </Box>
        </RouterLink>
      </Menu.Item>
      <Menu.Item key="page3">
        <RouterLink to="/page3">
          <Box hAligned>
            <AndroidOutlined />
            {!collapsed && <FormattedMessage {...messages.page3} />}
          </Box>
        </RouterLink>
      </Menu.Item>
    </Menu>
  );
};

const messages = defineMessages({
  login: {
    defaultMessage: 'Login',
  },
  home: {
    defaultMessage: 'Home',
  },
  page2: {
    defaultMessage: 'Page 2',
  },
  page3: {
    defaultMessage: 'Page 3',
  },
});

SideMenu.propTypes = {
  collapsed: PropTypes.bool,
};

SideMenu.defaultProps = {
  collapsed: false,
};

export default SideMenu;
