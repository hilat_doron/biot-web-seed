import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Layout, Avatar, Typography, Dropdown, Menu, Button } from 'antd';
import styled from 'styled-components';
import { injectIntl, defineMessages } from 'react-intl';

import { UserOutlined } from '@ant-design/icons';

import Logo from 'src/components/Logo';
import { Spacer, StyledHeader, MainContent, CollapseIcon, HeaderComponentsBox } from './AppLayout.styled';
import { StyledLayout } from '../../styled';
import SideMenu from './SideMenu';
import LanguageMenu from '../../LanguageMenu';

const { Sider } = Layout;
const MenuLogo = styled(Logo)`
  height: 60px;
  padding: 8px 8px 8px 0;
  margin: 0;
`;

const AppLayout = ({ username, logout, intl, children }) => {
  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  const userMenu = (
    <Menu>
      <Menu.Item>
        <Button type="link" onClick={logout}>
          {intl.formatMessage(messages.logout)}
        </Button>
      </Menu.Item>
    </Menu>
  );

  return (
    <StyledLayout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <MenuLogo small={collapsed} />
        <SideMenu collapsed={collapsed} />
      </Sider>
      <Layout>
        <StyledHeader>
          <HeaderComponentsBox gutter={12} hAligned>
            <CollapseIcon className="trigger" type={collapsed ? 'menu-unfold' : 'menu-fold'} onClick={toggle} />
            <Spacer />
            <Typography.Text>{username}</Typography.Text>
            <LanguageMenu />
            <Dropdown overlay={userMenu}>
              <Avatar icon={<UserOutlined />} />
            </Dropdown>
          </HeaderComponentsBox>
        </StyledHeader>
        <MainContent>{children}</MainContent>
      </Layout>
    </StyledLayout>
  );
};

const messages = defineMessages({
  logout: {
    defaultMessage: 'logout',
  },
});

AppLayout.propTypes = {
  username: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

export default injectIntl(AppLayout);
