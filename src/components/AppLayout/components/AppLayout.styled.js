import styled from 'styled-components';
import Box from 'src/components/Box';
import { Layout } from 'antd';
import { MenuUnfoldOutlined } from '@ant-design/icons';

const { Header, Content } = Layout;

export const StyledHeader = styled(Header).attrs({
  size: 'small',
})`
  background: ${props => props.theme.colors.headerBackground};
  color: ${props => props.theme.colors.headerTextColor};
  padding: 0;
`;

export const MainContent = styled(Content)`
  margin: 0;
  padding: 24px;
  background: ${props => props.theme.colors.MainContentBackground};
  min-height: 280px;
`;

export const HeaderComponentsBox = styled(Box).attrs({
  hAligned: true,
  gutter: 12,
})`
  padding-right: 20px;
`;

export const Spacer = styled.div`
  flex-grow: 2;
`;

export const CollapseIcon = styled(MenuUnfoldOutlined)`
  font-size: 18px;
  line-height: 64px;
  padding: 0 24px;
  cursor: pointer;
  transition: color 0.3s;
`;
