import { connect } from 'react-redux';
import { selectors as userSelectors } from 'src/redux/data/user/modules/slice';
import { actions as loginActions } from 'src/routes/Auth/Login/modules/slice';
import Component from '../components/Component';

const mapStateToProps = state => ({
  username: userSelectors.getUsername(state),
});

const mapActionCreators = {
  logout: loginActions.logout,
};

export default connect(mapStateToProps, mapActionCreators)(Component);
