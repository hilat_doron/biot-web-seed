/* eslint-disable no-console */
console.log('*********** react-app-rewired config loading ***********');
/* eslint-enable no-console */

const {
  addBabelPlugins,
  addDecoratorsLegacy,
  addLessLoader,
  addWebpackAlias,
  addWebpackPlugin,
  fixBabelImports,
  override,
  overrideDevServer,
  watchAll,
} = require('customize-cra');
const path = require('path');
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

module.exports = {
  webpack: override(
    // usual webpack plugin
    addDecoratorsLegacy(),

    // aliases to comply with react-boilerplate imports
    addWebpackAlias({
      src: path.resolve(__dirname, 'src'),
    }),

    // ant design: minify css bundle size
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),

    addLessLoader({
      javascriptEnabled: true,
      // modifyVars:        { '@primary-color': '#FF0000' },
    }),

    fixBabelImports('lodash', {
      libraryName: 'lodash',
      libraryDirectory: '',
      camel2DashComponentName: false,
    }),

    ...addBabelPlugins('babel-plugin-styled-components', [
      'formatjs',
      {
        idInterpolationPattern: '[sha512:contenthash:base64:6]',
        ast: true,
      },
    ]),
    addWebpackPlugin(new AntdDayjsWebpackPlugin()),
    addWebpackPlugin(new ProgressBarPlugin()),
  ),
  devServer: overrideDevServer(watchAll() /* dev server plugin */),
};
