@echo off

if "%1"=="" GOTO USAGE
if "%2"=="" GOTO USAGE

set S3_BUCKET_NAME=%1
set CF_DIST_ID=%2
set MSG="S3 Bucket: %S3_BUCKET_NAME%, CloudFront distribution ID: %CF_DIST_ID%"

CALL :print %MSG%

:PROMPT
SET /P AREYOUSURE=Are you sure (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

cd..
CALL :print "Deleting 'build' folder..."
rmdir /s /q build

CALL :print "Building web client..."
call yarn build


CALL :print "Syncing to s3 bucket..."
aws s3 sync ./build s3://%S3_BUCKET_NAME%

if ERRORLEVEL 1 GOTO Error


CALL :print "Creating invalidation in cloudfront..."
aws cloudfront create-invalidation --distribution-id %CF_DIST_ID% --paths "/*"

GOTO End

:Error
CALL :print "Error occured!!!"
GOTO End

:USAGE
echo Usage:
echo deploy [s3-bucket-name] [cloud-front-distribution-id]
GOTO END

:End
echo Exit code %ERRORLEVEL%
EXIT /B %ERRORLEVEL%



:print 
echo --------------------------------------------------------------------
echo %~1
echo --------------------------------------------------------------------
EXIT /B 0
