This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Welcome to biot-web-seed
Starting a new project is fun! 🎉 <br />
If this is your first time here, you're welcome to find out more about bio-t best practices for web projects, [here](https://softimize.atlassian.net/wiki/spaces/WIKI/pages/1929904129/FE+Best+Practices). <br />
You can also find our cool list of recommended VS-Code extensions, [here](https://softimize.atlassian.net/wiki/spaces/WIKI/pages/408060415/VS+Code+Extensions).

#
## React Intl
### Add/update messages
When adding new messages OR changing existing ones, run:
1. `npm run translations:extract` <br />
This should add/change your new/updated messages to _src\translations\messages\messages.json_
2. `npm run translations:manage` <br />
This should update all of the locale files with the new/updated messages. 
You should go over the locale files and translate the modified messages (see script's output).

### Add new locales

1. Change _languageUtils.js_: 
    - Add the new locale to `supportedLocales` (e.g `'fr_FR'` for french)
    - Add the new language to `supportedLanguages` (e.g `'fr'`)
2. Change _scripts\translate.js_:
    - Add the new language to `manageTranslations`'s languages array
3. Run `npm run translations:manage` - a new locale file (e.g fr.json) should be created under _src\translations\locales_
4. Change _src\translations\locales\index.js_: 
    - Add the new json file to export line
