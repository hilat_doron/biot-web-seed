# Version 0.0.2

**Released by**: Ben Rosencveig **Release date**: March 30, 2020

## Changes
- Added backend service.

# Version 0.0.1

**Released by**: Ben Rosencveig **Release date**: March 30, 2020

## Changes

- First version.
